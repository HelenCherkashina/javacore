package com.spring;

import java.util.EventListener;

public class Main {
    private static boolean ready;
    private static int number;

    public static void main(String[] args) {
        System.out.println(Runtime.getRuntime().availableProcessors());
    }
}

class ThisEscape {
    public ThisEscape(EventSource source) {
        ThisEscape that = this;
        source.registerListener(
                new EventListener() {
                    @Override
                    public int hashCode() {
                        return super.hashCode();
                    }

                    ThisEscape getThis() {
                        return that;
                    }
                }
        );
    }
}

class EventSource {
    private EventListener eventListener;

    void registerListener(EventListener eventListener) {
        this.eventListener = eventListener;
    }

    EventListener getListener() {
        return eventListener;
    }
}