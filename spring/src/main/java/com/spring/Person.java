package com.spring;

import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class Person {
    @Generate
    private String name;

    public Person() {
        System.out.println("Constructor");
    }

    @PostConstruct
    public void init() {
        System.out.println("Init");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
