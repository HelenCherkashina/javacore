package com.cherkashyna.scraper.tasks;

import com.cherkashyna.scraper.connectors.JSoupConnector;
import com.cherkashyna.scraper.model.Link;
import com.cherkashyna.scraper.model.Page;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.RecursiveTask;
import java.util.stream.Collectors;

import static org.apache.commons.lang3.StringUtils.EMPTY;

public class PageScraperTask extends RecursiveTask<Page> {
    private static final Logger LOGGER = Logger.getLogger(PageScraperTask.class);

    private static final String LAYOUT_CONTENT = "layout-content";
    private static final String LINK_TAG = "a";
    private static final String LINK_ABSOLUTE_URL = "abs:href";

    private final JSoupConnector connector;

    private final String pageName;
    private final String url;
    private final Map<String, Page> siteStructure;

    public PageScraperTask(String pageName, String url, Map<String, Page> siteStructure) {
        this.url = url;
        this.pageName = pageName;
        this.siteStructure = Collections.synchronizedMap(siteStructure);
        this.connector = new JSoupConnector();
    }

    @Override
    protected Page compute() {
        LOGGER.info("Thread: " + Thread.currentThread().getName() + "; Page: " + url);
        if(siteStructure.containsKey(url)) {
            return null;
        }

        return connector.getHtmlDocument(url)
                .map(this::getDocumentPage)
                .orElse(new Page(pageName, url, EMPTY));
    }

    private Page getDocumentPage(Document document) {
        Elements layoutElements = document.getElementsByClass(LAYOUT_CONTENT);

        if(layoutElements.isEmpty()) {
            return new Page(pageName, url, EMPTY);
        }

        Element content = layoutElements.get(0);
        Page page = new Page(pageName, url, content.outerHtml());
        siteStructure.put(url, page);
        Set<Link> linksOnThePage = content.getElementsByTag(LINK_TAG)
                .stream()
                .filter(linkTag -> StringUtils.isNotEmpty(linkTag.attr(LINK_ABSOLUTE_URL)))
                .map(linkTag -> new Link(linkTag.text(), linkTag.attr(LINK_ABSOLUTE_URL)))
                .collect(Collectors.toSet());

        List<PageScraperTask> taskForChildPages = linksOnThePage.stream()
                .map(link -> new PageScraperTask(link.getName(), link.getUrl(), siteStructure))
                .collect(Collectors.toList());

        taskForChildPages.forEach(task -> {
            task.fork();
            page.addChildPage(task.join());
        });

        return page;
    }
}
