package com.cherkashyna.scraper.tasks;

import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.RecursiveTask;

public class SortTask extends RecursiveTask<List<Integer>> {
    private static final Logger LOGGER = Logger.getLogger(SortTask.class);

    private static final int THRESHOLD = 2;

    private final List<Integer> numbers;

    public SortTask(List<Integer> numbers) {
        this.numbers = numbers;
    }

    @Override
    protected List<Integer> compute() {
        LOGGER.info("Thread: " + Thread.currentThread().getName());
        if (numbers.size() > THRESHOLD) {
            return createSubtasks();
        } else {
            return sort();
        }
    }

    private List<Integer> createSubtasks() {
        List<Integer> firstPart = numbers.subList(0, numbers.size() / 2);
        List<Integer> secondTask = numbers.subList(numbers.size() / 2, numbers.size());

        List<Integer> leftSideArray = new SortTask(firstPart).fork().join();
        List<Integer> rightSideArray = new SortTask(secondTask).fork().join();

        return merge(leftSideArray, rightSideArray);
    }

    private List<Integer> sort() {
        if (numbers.size() == 1) {
            return numbers;
        }

        if (numbers.get(0) > numbers.get(1)) {
            Integer temp = numbers.get(0);
            numbers.set(0, numbers.get(1));
            numbers.set(1, temp);
            return numbers;
        }

        return numbers;
    }

    private List<Integer> merge(List<Integer> firstArray, List<Integer> secondArray) {
        List<Integer> result = new ArrayList<>();

        if (firstArray.size() == 0) {
            result.addAll(secondArray);
            return result;
        }

        if (secondArray.size() == 0) {
            result.addAll(firstArray);
            return result;
        }


        if (firstArray.size() >= secondArray.size()) {
            return mergeArrays(firstArray, secondArray);
        }

        return mergeArrays(secondArray, firstArray);
    }

    private List<Integer> mergeArrays(List<Integer> firstArray, List<Integer> secondArray) {
        List<Integer> result = new ArrayList<>();

        Iterator<Integer> firstArrayIterator = firstArray.iterator();
        Iterator<Integer> secondArrayIterator = secondArray.iterator();

        while (firstArrayIterator.hasNext()) {
            Integer firstArrayElement = firstArrayIterator.next();
            while (secondArrayIterator.hasNext()) {
                Integer secondArrayElement = secondArrayIterator.next();

                if (firstArrayElement < secondArrayElement) {
                    result.add(firstArrayElement);
                    break;
                } else {
                    result.add(secondArrayElement);
                }
            }

            result.add(firstArrayElement);
        }

        return result;
    }
}
