package com.cherkashyna.scraper.model;

import java.util.ArrayList;
import java.util.List;

public class Page {
    private final String name;
    private final String url;
    private final String content;
    private List<Page> children;

    public Page(String name, String url, String content) {
        this.name = name;
        this.url = url;
        this.content = content;
        this.children = new ArrayList<>();
    }

    public synchronized void addChildPage(Page page) {
        children.add(page);
    }

    public String getName() {
        return name;
    }

    public String getUrl() {
        return url;
    }

    public String getContent() {
        return content;
    }

    public List<Page> getChildren() {
        return children;
    }

    @Override
    public String toString() {
        return "Page{" +
                "name='" + name + '\'' +
                ", url='" + url + '\'' +
                ", content='" + content + '\'' +
                ", children=" + children +
                '}';
    }
}
