package com.cherkashyna.scraper;

import com.cherkashyna.scraper.tasks.SortTask;

import java.util.List;
import java.util.Random;
import java.util.concurrent.ForkJoinPool;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Main {
    public static void main(String[] args) {
        Random rand = new Random();
        List<Integer> numbers = IntStream.generate(() -> rand.nextInt(11)).limit(10).boxed().collect(Collectors.toList());
        System.out.println(numbers);
        List<Integer> sortedList = new ForkJoinPool().invoke(new SortTask(numbers));
        System.out.println(sortedList);
    }
}