package com.cherkashyna.scraper.connectors;

import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.util.Optional;

public class JSoupConnector {
    private static final Logger LOGGER = Logger.getLogger(JSoupConnector.class);

    public Optional<Document> getHtmlDocument(String url) {
        try {
            return Optional.of(Jsoup.connect(url).get());
        } catch (IOException e) {
            LOGGER.error("Can't connect to the " + url + " page");
            return Optional.empty();
        }
    }
}
