package com.collections;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(Parameterized.class)
public class CustomListsTest {
    @Parameterized.Parameter
    public Class clazz;

    private List<String> testStrings;

    @Parameterized.Parameters
    public static Collection<Class> data() {
        return Arrays.asList(CustomArrayList.class, CustomLinkedList.class);
    }

    @Before
    public void setUp() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        this.testStrings = (List<String>) CustomArrayList.class.getDeclaredConstructor().newInstance();
    }

    @Test
    public void shouldReturnTrueIfEmpty() {
        //given

        //when

        //then
        assertTrue(testStrings.isEmpty());
    }

    @Test
    public void shouldAddToList() {
        //given

        //when
        testStrings.add("first");
        testStrings.add("last");

        //then
        assertEquals("first", testStrings.get(0));
        assertEquals("last", testStrings.get(1));
    }

    @Test
    public void shouldAddToListWithEnsuringCapacity() {
        //given

        //when
        testStrings.add("first");
        testStrings.add("second");
        testStrings.add("third");
        testStrings.add("fourth");
        testStrings.add("fifth");
        testStrings.add("sixth");
        testStrings.add("seventh");
        testStrings.add("eighth");
        testStrings.add("ninth");
        testStrings.add("tenth");
        testStrings.add("eleventh");

        //then
        assertEquals(11, testStrings.size());
    }

    @Test
    public void shouldRemoveFromList() {
        //given
        testStrings.add("first");
        testStrings.add("second");
        testStrings.add("last");

        //when
        testStrings.remove("second");

        //then
        assertEquals(2, testStrings.size());
        assertEquals("first", testStrings.get(0));
        assertEquals("last", testStrings.get(1));
    }

    @Test
    public void shouldGetAnElement() {
        //given
        testStrings.add("first");
        testStrings.add("second");
        testStrings.add("third");
        testStrings.add("fourth");
        testStrings.add("fifth");
        testStrings.add("sixth");
        testStrings.add("seventh");
        testStrings.add("eighth");
        testStrings.add("ninth");
        testStrings.add("tenth");
        testStrings.add("eleventh");

        //when
        String testString = testStrings.get(4);

        //then
        assertEquals("fifth", testString);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowAnExceptionIfIndexOutOfArrayRange() {
        //given
        testStrings.add("first");
        testStrings.add("second");
        testStrings.add("third");
        testStrings.add("fourth");
        testStrings.add("fifth");
        testStrings.add("sixth");
        testStrings.add("seventh");
        testStrings.add("eighth");
        testStrings.add("ninth");
        testStrings.add("tenth");
        testStrings.add("eleventh");

        //when
        testStrings.get(65);

        //then
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowAnExceptionIfIndexIsNegative() {
        //given
        testStrings.add("first");
        testStrings.add("second");
        testStrings.add("third");
        testStrings.add("fourth");
        testStrings.add("fifth");
        testStrings.add("sixth");
        testStrings.add("seventh");
        testStrings.add("eighth");
        testStrings.add("ninth");
        testStrings.add("tenth");
        testStrings.add("eleventh");

        //when
        testStrings.get(-4);

        //then
    }

    @Test
    public void shouldAddAllElements() {
        //given
        testStrings.add("first");
        testStrings.add("second");
        testStrings.add("third");

        //when
        testStrings.addAll(Arrays.asList("one", "two", "three"));

        //then
        assertEquals(6, testStrings.size());
        assertEquals("first", testStrings.get(0));
        assertEquals("second", testStrings.get(1));
        assertEquals("third", testStrings.get(2));
        assertEquals("one", testStrings.get(3));
        assertEquals("two", testStrings.get(4));
        assertEquals("three", testStrings.get(5));
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowExceptionIfAddedCollectionIsNull() {
        //given
        testStrings.add("first");
        testStrings.add("second");
        testStrings.add("third");

        //when
        testStrings.addAll(null);

        //then
        assertEquals(3, testStrings.size());
        assertEquals("first", testStrings.get(0));
        assertEquals("second", testStrings.get(1));
        assertEquals("third", testStrings.get(2));
    }

    @Test
    public void shouldReturnIndexOfElement() {
        //given
        testStrings.add("first");
        testStrings.add("second");
        testStrings.add("third");

        //when
        int index = testStrings.indexOf("second");

        //then
        assertEquals(1, index);
    }

    @Test
    public void shouldReturnIndexOfNullElement() {
        //given
        testStrings.add("first");
        testStrings.add("second");
        testStrings.add(null);

        //when
        int index = testStrings.indexOf(null);

        //then
        assertEquals(2, index);
    }

    @Test
    public void shouldReturnNegativeIndexNotExistingElement() {
        //given
        testStrings.add("first");
        testStrings.add(null);
        testStrings.add("third");

        //when
        int index = testStrings.indexOf("five");

        //then
        assertEquals(-1, index);
    }

    @Test
    public void shouldReturnTrueIfElementExists() {
        //given
        testStrings.add("first");
        testStrings.add(null);
        testStrings.add("third");

        //when

        //then
        assertTrue(testStrings.contains(null));
    }

    @Test
    public void shouldReturnFalseIfElementNotExists() {
        //given
        testStrings.add("first");
        testStrings.add("second");
        testStrings.add("third");

        //when

        //then
        assertFalse(testStrings.contains("five"));
    }

    @Test
    public void shouldReturnTrueIfContainsCollection() {
        //given
        testStrings.add("first");
        testStrings.add("second");
        testStrings.add("third");

        //when

        //then
        assertTrue(testStrings.containsAll(Arrays.asList("second", "third")));
    }

    @Test
    public void shouldReturnFalseIfNotContainsCollection() {
        //given
        testStrings.add("first");
        testStrings.add("second");
        testStrings.add("third");

        //when

        //then
        assertFalse(testStrings.containsAll(Arrays.asList("second", "third", "five")));
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowExceptionIfCollectionIsNull() {
        //given
        testStrings.add("first");
        testStrings.add("second");
        testStrings.add("third");

        //when

        //then
        assertFalse(testStrings.containsAll(null));
    }

    @Test
    public void shouldAddCollectionToPosition() {
        //given
        testStrings.add("first");
        testStrings.add("second");
        testStrings.add("third");

        //when
        testStrings.addAll(1, Arrays.asList("one", "two", "three", "four", "five"));

        //then
        assertEquals(8, testStrings.size());
        assertEquals("first", testStrings.get(0));
        assertEquals("one", testStrings.get(1));
        assertEquals("two", testStrings.get(2));
        assertEquals("three", testStrings.get(3));
        assertEquals("four", testStrings.get(4));
        assertEquals("five", testStrings.get(5));
        assertEquals("second", testStrings.get(6));
        assertEquals("third", testStrings.get(7));
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowExceptionIfAddedCollectionToPositionIsNull() {
        //given
        testStrings.add("first");
        testStrings.add("second");
        testStrings.add("third");

        //when
        testStrings.addAll(1, null);

        //then
        assertEquals(3, testStrings.size());
        assertEquals("first", testStrings.get(0));
        assertEquals("second", testStrings.get(1));
        assertEquals("third", testStrings.get(2));
    }


    @Test
    public void shouldAddElementToPosition() {
        //given
        testStrings.add("first");
        testStrings.add("second");
        testStrings.add("third");

        //when
        testStrings.add(1, "one");

        //then
        assertEquals(4, testStrings.size());
        assertEquals("first", testStrings.get(0));
        assertEquals("one", testStrings.get(1));
        assertEquals("second", testStrings.get(2));
        assertEquals("third", testStrings.get(3));
    }

    @Test
    public void shouldRemoveExistingList() {
        //given
        testStrings.add("first");
        testStrings.add("second");
        testStrings.add("third");
        testStrings.add("forth");
        testStrings.add("fifth");

        //when
        testStrings.removeAll(Arrays.asList("second", "fifth"));

        //then
        assertEquals(3, testStrings.size());
        assertEquals("first", testStrings.get(0));
        assertEquals("third", testStrings.get(1));
        assertEquals("forth", testStrings.get(2));
    }

    @Test
    public void shouldRemoveExistingElement() {
        //given
        testStrings.add("first");
        testStrings.add("second");
        testStrings.add("third");
        testStrings.add("forth");
        testStrings.add("fifth");

        //when
        testStrings.remove(0);

        //then
        assertEquals(4, testStrings.size());
        assertEquals("second", testStrings.get(0));
        assertEquals("third", testStrings.get(1));
        assertEquals("forth", testStrings.get(2));
        assertEquals("fifth", testStrings.get(3));
    }

    @Test
    public void shouldReturnLastIndexOfElement() {
        //given
        testStrings.add("first");
        testStrings.add("second");
        testStrings.add("third");
        testStrings.add("second");
        testStrings.add("fifth");

        //when
        int index = testStrings.lastIndexOf("second");

        //then
        assertEquals(3, index);
    }
}