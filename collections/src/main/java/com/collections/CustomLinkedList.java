package com.collections;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class CustomLinkedList<E> implements List<E> {

    private Node<E> head;
    private Node<E> tail;
    private int size;

    @Override
    public int size() {
        return this.size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean contains(Object o) {
        return indexOf(o) != -1;
    }

    @Override
    public Iterator<E> iterator() {
        return null;
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return null;
    }

    @Override
    public boolean add(E e) {
        linkToLastNode(e);
        return true;
    }

    @Override
    public boolean remove(Object o) {
        Node<E> currentNode = head;

        if (o == null) {
            for (int i = 0; i < size; i++) {
                if (currentNode.getElement() == null) {
                    unlinkNode(currentNode);
                    return true;
                }
                currentNode = currentNode.getNext();
            }
        } else {
            for (int i = 0; i < size; i++) {
                if (o.equals(currentNode.getElement())) {
                    unlinkNode(currentNode);
                    return true;
                }
                currentNode = currentNode.getNext();
            }
        }
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        collectionIsEmpty(c);

        for (Object e : c) {
            if (!contains(e)) {
                return false;
            }
        }

        return true;
    }

    @Override
    public boolean addAll(Collection<? extends E> c) {
        collectionIsEmpty(c);

        for (E element : c) {
            linkToLastNode(element);
        }
        return true;
    }

    @Override
    public boolean addAll(int index, Collection<? extends E> c) {
        indexInRange(index);

        collectionIsEmpty(c);

        if(index == 0) {
            for (E element : c) {
                linkBeforeFirstNode(element);
            }
            size += c.size();
            return true;
        }

        if(index == size) {
            for (E element : c) {
                linkToLastNode(element);
            }
            size += c.size();
            return true;
        }

        Node<E> prev = findNodeByIndex(index - 1);
        Node<E> next = findNodeByIndex(index);

        Node<E> currentNode;

        for (Object o : c) {
            currentNode = new Node<>((E)o, prev, next);
            prev.setNext(currentNode);
            prev = currentNode;
        }

        size += c.size();
        return true;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        collectionIsEmpty(c);

        Node<E> currentNode = head;
        while(currentNode != null) {
            for (Object element : c) {
                if(element == null) {
                    if (currentNode.getElement() == null) {
                        unlinkNode(currentNode);
                        size--;
                    }
                } else {
                    if (element.equals(currentNode.getElement())) {
                        unlinkNode(currentNode);
                        size--;
                    }
                }
            }
            currentNode = currentNode.getNext();
        }

        return true;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return false;
    }

    @Override
    public void clear() {
        Node<E> currentNode = head;
        for (int i = 0; i < size; i++) {
            currentNode = null;
        }

        size = 0;
    }

    @Override
    public E get(int index) {
        indexInRange(index);

        return findNodeByIndex(index).getElement();
    }

    @Override
    public E set(int index, E element) {
        return null;
    }

    @Override
    public void add(int index, E element) {
        indexInRange(index);

        if(index == 0) {
            linkBeforeFirstNode(element);
            size++;
        }

        if(index == size) {
            linkToLastNode(element);
            size++;
        }

        Node<E> prev = findNodeByIndex(index - 1);
        Node<E> next = findNodeByIndex(index);

        Node<E> newNode = new Node<>(element, prev, next);
        prev.setNext(newNode);
        next.setPrev(newNode);

        size++;
    }

    @Override
    public E remove(int index) {
        Node<E> currentNode = findNodeByIndex(index);
        return unlinkNode(currentNode);
    }

    @Override
    public int indexOf(Object o) {
        Node<E> currentNode = head;

        if (o == null) {
            for (int i = 0; i < size; i++) {
                if (currentNode.getElement() == null) {
                    return i;
                }
                currentNode = currentNode.getNext();
            }
        } else {
            for (int i = 0; i < size; i++) {
                if (o.equals(currentNode.getElement())) {
                    return i;
                }
                currentNode = currentNode.getNext();
            }
        }
        return -1;
    }

    @Override
    public int lastIndexOf(Object o) {
        Node<E> currentNode = tail;

        if (o == null) {
            for (int i = size - 1; i > 0; i--) {
                if (currentNode.getElement() == null) {
                    return i;
                }
                currentNode = currentNode.getPrev();
            }
        } else {
            for (int i = size - 1; i > 0; i--) {
                if (o.equals(currentNode.getElement())) {
                    return i;
                }
                currentNode = currentNode.getPrev();
            }
        }
        return -1;
    }

    @Override
    public ListIterator<E> listIterator() {
        return null;
    }

    @Override
    public ListIterator<E> listIterator(int index) {
        return null;
    }

    @Override
    public List<E> subList(int fromIndex, int toIndex) {
        return null;
    }

    private void indexInRange(int index) {
        if (index < 0 || index > size) {
            throw new IllegalArgumentException("Index is out of array");
        }
    }

    private void linkBeforeFirstNode(E e) {
        Node<E> start = this.head;
        Node<E> newNode = new Node<>(e, null, start);
        if (head == null) {
            this.head = newNode;
            this.tail = newNode;
        } else {
            start.setPrev(newNode);
            this.head = newNode;
        }

        size++;
    }

    private void linkToLastNode(E e) {
        Node<E> last = this.tail;
        Node<E> newNode = new Node<>(e, last, null);
        if (head == null) {
            this.head = newNode;
            this.tail = newNode;
        } else {
            last.setNext(newNode);
            this.tail = newNode;
        }

        size++;
    }

    private E unlinkNode(Node<E> nodeToRemove) {
        E element = nodeToRemove.getElement();
        Node<E> next = nodeToRemove.getNext();
        Node<E> prev = nodeToRemove.getPrev();

        if (next == null) {
            tail = prev;
        } else {
            next.setPrev(prev);
        }

        if (prev == null) {
            head = next;
        } else {
            prev.setNext(next);
        }

        nodeToRemove.setPrev(null);
        nodeToRemove.setNext(null);
        nodeToRemove.setElement(null);

        size--;
        return element;
    }

    private Node<E> findNodeByIndex(int index) {
        Node<E> currentNode = head;
        for (int i = 0; i < index; i++) {
            currentNode = currentNode.getNext();
        }
        return currentNode;
    }

    private void collectionIsEmpty(Collection<?> c) {
        if(c == null) {
            throw new IllegalArgumentException("Added collection should not be empty");
        }
    }

    private class Node<E> {
        private E element;
        private Node<E> prev;
        private Node<E> next;

        public Node(E element, Node<E> prev, Node<E> next) {
            this.element = element;
            this.prev = prev;
            this.next = next;
        }

        public E getElement() {
            return element;
        }

        public Node<E> getPrev() {
            return prev;
        }

        public Node<E> getNext() {
            return next;
        }

        public void setElement(E element) {
            this.element = element;
        }

        public void setPrev(Node<E> prev) {
            this.prev = prev;
        }

        public void setNext(Node<E> next) {
            this.next = next;
        }
    }
}
