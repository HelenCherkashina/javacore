package com.collections;

import org.openjdk.jmh.annotations.*;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;

@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.MICROSECONDS)
@Warmup(iterations = 10)
public class LinkedListBenchmark {
    @State(Scope.Thread)
    public static class ExecutionPlan {

        @Param({"100", "1000"})
        private int iterations;

        public List<Integer> numbers;

        @Setup(Level.Iteration)
        public void setUp() {
            this.numbers = new LinkedList<>();
            for (int i = 0; i < iterations; i++) {
                numbers.add(i);
            }

            numbers.indexOf(100);
        }
    }

    @Benchmark
    public void addElement(LinkedListBenchmark.ExecutionPlan plan) {
        plan.numbers.add(1);
    }

    @Benchmark
    public void addElementToTheMiddle(LinkedListBenchmark.ExecutionPlan plan) {
        plan.numbers.add(plan.iterations / 2, 1);
    }

    @Benchmark
    public int testIndexOf(LinkedListBenchmark.ExecutionPlan plan) {
        return plan.numbers.indexOf(plan.iterations / 2);
    }

    @Benchmark
    public Integer getElement(LinkedListBenchmark.ExecutionPlan plan) {
        return plan.numbers.get(plan.iterations / 2);
    }

    @Benchmark
    public Integer removeElement(LinkedListBenchmark.ExecutionPlan plan) {
        return plan.numbers.remove(plan.iterations / 2);
    }
}
