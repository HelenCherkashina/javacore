package com.collections;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class CustomArrayList<T> implements List<T> {
    private static final int DEFAULT_CAPACITY = 10;

    private int size;

    private Object[] array;

    public CustomArrayList() {
        this.array = new Object[DEFAULT_CAPACITY];
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean contains(Object o) {
        return indexOf(o) != -1;
    }

    @Override
    public Iterator<T> iterator() {
        return null;
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        return null;
    }

    @Override
    public boolean add(T t) {
        if (size == array.length) {
            ensureCapacity();
        }
        array[size++] = t;
        return true;
    }

    private void ensureCapacity() {
        Object[] newArray = new Object[size * 2];
        System.arraycopy(array, 0, newArray, 0, size);
        this.array = newArray;
    }

    @Override
    public boolean remove(Object o) {
        int index = indexOf(o);

        indexInRange(index);

        array[index] = null;
        System.arraycopy(array, index + 1, array, index, size - index - 1);
        size--;
        return true;
    }

    private void indexInRange(int index) {
        if (index < 0 || index > size) {
            throw new IllegalArgumentException("Index is out of array");
        }
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        collectionIsEmpty(c);

        for (Object object : c) {
            if (!contains(object)) {
                return false;
            }
        }

        return true;
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        collectionIsEmpty(c);

        int newSize = c.size() + this.size;
        Object[] newArray = new Object[newSize];
        System.arraycopy(array, 0, newArray, 0, size);
        System.arraycopy(c.toArray(), 0, newArray, size, c.size());
        this.array = newArray;
        this.size = newSize;
        return true;
    }

    @Override
    public boolean addAll(int index, Collection<? extends T> c) {
        collectionIsEmpty(c);

        indexInRange(index);

        int newSize = c.size() + this.size;
        Object[] newArray = new Object[newSize];
        System.arraycopy(array, 0, newArray, 0, index);
        System.arraycopy(c.toArray(), 0, newArray, index, c.size());
        System.arraycopy(array, index, newArray, c.size() + index, size - index);
        this.array = newArray;
        this.size = newSize;
        return true;
    }

    private void collectionIsEmpty(Collection<?> c) {
        if(c == null) {
            throw new IllegalArgumentException("Added collection should not be empty");
        }
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        collectionIsEmpty(c);

        Object[] newArray = new Object[array.length];

        int index = 0;
        for (int i = 0; i < size; i++) {
            if(!c.contains(array[i])) {
                newArray[index] = array[i];
                index++;
            }
        }

        this.array = newArray;
        this.size = index;

        return true;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return false;
    }

    @Override
    public void clear() {
        for (int i = 0; i <= size; i++) {
            array[i] = null;
        }

        size = 0;
    }

    @Override
    public T get(int index) {
        indexInRange(index);
        return (T) array[index];
    }

    @Override
    public T set(int index, T element) {
        return null;
    }

    @Override
    public void add(int index, T element) {
        indexInRange(index);

        int newSize = size + 1;
        Object[] newArray = new Object[newSize];
        System.arraycopy(array, 0, newArray, 0, index);
        System.arraycopy(array, index, newArray, index + 1, size - index);
        newArray[index] = element;
        this.array = newArray;
        this.size = newSize;
    }

    @Override
    public T remove(int index) {
        indexInRange(index);

        Object[] newArray = new Object[size - 1];

        Object element = array[index];

        System.arraycopy(array, 0, newArray, 0, index);
        System.arraycopy(array, index + 1, newArray, index, size - index - 1);

        this.array = newArray;
        this.size = newArray.length;

        return (T) element;
    }

    @Override
    public int indexOf(Object o) {
        for (int i = 0; i <= size; i++) {
            if (o == null) {
                if (o == array[i]) {
                    return i;
                }
            } else {
                if (o.equals(array[i])) {
                    return i;
                }
            }
        }
        return -1;
    }

    @Override
    public int lastIndexOf(Object o) {
        for (int i = size; i >= 0; i--) {
            if (o == null) {
                if (o == array[i]) {
                    return i;
                }
            } else {
                if (o.equals(array[i])) {
                    return i;
                }
            }
        }
        return -1;
    }

    @Override
    public ListIterator<T> listIterator() {
        return null;
    }

    @Override
    public ListIterator<T> listIterator(int index) {
        return null;
    }

    @Override
    public List<T> subList(int fromIndex, int toIndex) {
        indexInRange(fromIndex);
        indexInRange(toIndex);

        if(toIndex < fromIndex) {
            throw new IllegalArgumentException("Invalid range values");
        }

        Object[] newArray = new Object[toIndex - fromIndex + 1];

        int index = 0;
        for(int i = fromIndex; i <= toIndex; i++) {
            newArray[index] = array[i];
            index++;
        }
        return null;
    }
}
